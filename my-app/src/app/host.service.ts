import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HostService {
  serverUrl = undefined;
  clientUrl = undefined;

  constructor(private router: Router) {
    this.init();
  }

  init(): void {
    if (document.location.origin.indexOf('app.realwebsite.com') !== -1) { // production
      this.serverUrl = 'https://somewhereouthere.com/';
      this.clientUrl = 'https://app.realwebsite.com/';
    } else if (document.location.origin.indexOf('stage') !== -1) { // stage
      // https:// staging urls
    } else { // local :)
      this.serverUrl = 'http://localhost:3000/';
      this.clientUrl = 'http://' + document.location.host + '/';
    }
  }
}
