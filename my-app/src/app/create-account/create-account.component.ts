import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {

  userData = {
    email: '',
    username: '',
    password: '',
    confirm_password: '',
    first_name: '',
    last_name: '',
  };

  error = '';

  constructor(private userService: UserService, private router:Router) { }

  ngOnInit() {
  }

  createAccount(): void {
    this.userService.createAccount(this.userData)
    .subscribe(
      response => {
        if (response.hasOwnProperty('token')) {
          this.userService.setUserData(response);
          this.router.navigate(['/dashboard']);
        }
      },
      err => {
        console.log('Incorrect or Unrecognized Credentials.');
      }
    );
  }

}
