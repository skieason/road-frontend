import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HostService } from './host.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public user:any = {};

  constructor(private http: HttpClient, private hostInfo: HostService) { }

  isLoggedIn(): boolean {
    if (this.user.hasOwnProperty('user_id')) {
      return true;
    } else {
      return false;
    }
  }

  getUserData(): any {
    return this.user;
  }

  setUserData(userData): void {
    localStorage.setItem('jwt', userData.token);
    this.user = userData.data;
  }

  createAccount(userData): Observable<any> {
    return this.http.post(this.hostInfo.serverUrl + 'create-account', userData);
  }

  login(userData): Observable<any> {
    return this.http.post(this.hostInfo.serverUrl + 'authenticate', userData);
  }

  logout(): void {
    localStorage.removeItem('jwt');
    this.user = {};
  }

  verifyJWT(): Observable<any> {
    return this.http.post(this.hostInfo.serverUrl + 'api/verifyJWT', {}, {
      headers: new HttpHeaders().set('jwt-token', localStorage.getItem('jwt')),
    });
  }

  // emailResetPassword(password, token): Observable<any> {
  //   return this.http.put(this.hostInfo.serverUrl + 'api/email-update-password', {password: password}, {
  //     headers: new HttpHeaders().set('jwt-token', token)
  //   });
  // }
  //
  // sendPWResetEmail(email): Observable<any> {
  //   return this.http.post(this.hostInfo.serverUrl + 'send-password-reset', {email: email});
  // }
}
