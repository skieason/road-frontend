import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  reset = false;

  userData = {
    email: '',
    password: ''
  };

  error = '';

  constructor(private userService: UserService, private router:Router) { }

  ngOnInit() {
  }

  onKeydown() {
    this.login();
  }

  login(): void {
    this.error = '';
    this.userService.login(this.userData)
    .subscribe(
      response => {
        if (response.hasOwnProperty('token')) {
          this.userService.setUserData(response);
          this.router.navigate(['/dashboard']);
        }
      },
      err => {
        this.error = 'Incorrect password or unrecognized email.';
      }
    );
  }
}
