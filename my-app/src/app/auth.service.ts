import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HostService } from './host.service';

export const TOKEN_NAME: string = 'jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private hostInfo: HostService) { }

  getToken(): string {
    return localStorage.getItem(TOKEN_NAME);
  }

  setToken(token: string): void {
    localStorage.setItem(TOKEN_NAME, token);
  }

  isTokenExpired(): boolean {
    var token = this.getToken();
    if(!token) return true;

    const helper = new JwtHelperService();
    return helper.isTokenExpired(token);
  }

  decodeToken(): any {
    var token = this.getToken();
    if(!token) return true;

    const helper = new JwtHelperService();
    return helper.decodeToken(token);
  }
}
