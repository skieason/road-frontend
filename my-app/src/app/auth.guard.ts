import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './user.service';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private userService: UserService,
    private authService: AuthService) {}

  canActivate() {
    if (!this.authService.isTokenExpired()) {
      var userData = this.authService.decodeToken();
      delete userData.exp;
      delete userData.iat;
      this.userService.setUserData({data: userData, token: this.authService.getToken()});
      this.userService.verifyJWT()
      .subscribe(
        response => {
          if (response.hasOwnProperty('token')) {
            this.userService.setUserData(response);
            if (!this.userService.getUserData().stripe_user_id && !this.userService.getUserData().is_coach) {
              this.router.navigate(['/subscribe']);
              return true;
            }
          }
        },
        err => {
          console.log('Incorrect or No Token Given.');
          // this.router.navigate(['/login']);
        }
      );
      return true;
    }

    this.router.navigate(['/login']);
    return false;
  }
}
