import { Component, OnInit } from '@angular/core';
import { OrderByPipe } from '../order-by.pipe';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  selectedSegment = {
    id: null,
    score: null,
    image: null,
    details: [],
  };

  segments = [{
    id: 9129390,
    score: 5,
    date: new Date(),
    image: '../../assets/alligator-crack.jpg',
    details: [{
      type: 'Alligator Cracking',
      description: 'Cracks forming a pattern that looks like scales',
      accuracy: 91
    }, {
      type: 'Longitudinal Cracking',
      description: 'Cracking that runs parallel along the road',
      accuracy: 40
    }, {
      type: 'Slippage Cracking',
      description: 'Crescent or half-moon shaped cracks',
      accuracy: 30
    }, {
      type: 'Transverse Cracking',
      description: 'Thermal cracks perpendicular to the pavement’s centerline',
      accuracy: 30
    }, {
      type: 'Asphalt Depression',
      description: 'Areas with slightly lower elevations than the surrounding pavement',
      accuracy: 21
    }, {
      type: 'Asphalt Upheaval',
      description: 'Localized upward movement in a pavement',
      accuracy: 23
    }, {
      type: 'Joint Reflection',
      description: 'Cracks in a flexible overlay over an existing crack or joint',
      accuracy: 15
    }, {
      type: 'Pot Hole',
      description: 'A depression or hollow in a road surface',
      accuracy: 5
    }, {
      type: 'Raveling',
      description: 'Deterioration of surface asphalt',
      accuracy: 34
    }, {
      type: 'Rutting',
      description: 'A depression or groove worn into a road',
      accuracy: 39
    }]
  }, {
    id: 9129391,
    score: 7,
    date: new Date(),
    image: '../../assets/asphalt-depression.jpg',
    details: [{
      type: 'Alligator Cracking',
      description: 'Cracks forming a pattern that looks like scales',
      accuracy: 8
    }, {
      type: 'Longitudinal Cracking',
      description: 'Cracking that runs parallel along the road',
      accuracy: 65
    }, {
      type: 'Slippage Cracking',
      description: 'Crescent or half-moon shaped cracks',
      accuracy: 53
    }, {
      type: 'Transverse Cracking',
      description: 'Thermal cracks perpendicular to the pavement’s centerline',
      accuracy: 42
    }, {
      type: 'Asphalt Depression',
      description: 'Areas with slightly lower elevations than the surrounding pavement',
      accuracy: 92
    }, {
      type: 'Asphalt Upheaval',
      description: 'Localized upward movement in a pavement',
      accuracy: 71
    }, {
      type: 'Joint Reflection',
      description: 'Cracks in a flexible overlay over an existing crack or joint',
      accuracy: 40
    }, {
      type: 'Pot Hole',
      description: 'A depression or hollow in a road surface',
      accuracy: 79
    }, {
      type: 'Raveling',
      description: 'Deterioration of surface asphalt',
      accuracy: 14
    }, {
      type: 'Rutting',
      description: 'A depression or groove worn into a road',
      accuracy: 75
    }]
  }, {
    id: 9129392,
    score: 7,
    date: new Date(),
    image: '../../assets/asphalt-upheaval.jpg',
    details: [{
      type: 'Alligator Cracking',
      description: 'Cracks forming a pattern that looks like scales',
      accuracy: 5
    }, {
      type: 'Longitudinal Cracking',
      description: 'Cracking that runs parallel along the road',
      accuracy: 85
    }, {
      type: 'Slippage Cracking',
      description: 'Crescent or half-moon shaped cracks',
      accuracy: 3
    }, {
      type: 'Transverse Cracking',
      description: 'Thermal cracks perpendicular to the pavement’s centerline',
      accuracy: 15
    }, {
      type: 'Asphalt Depression',
      description: 'Areas with slightly lower elevations than the surrounding pavement',
      accuracy: 13
    }, {
      type: 'Asphalt Upheaval',
      description: 'Localized upward movement in a pavement',
      accuracy: 92
    }, {
      type: 'Joint Reflection',
      description: 'Cracks in a flexible overlay over an existing crack or joint',
      accuracy: 8
    }, {
      type: 'Pot Hole',
      description: 'A depression or hollow in a road surface',
      accuracy: 3
    }, {
      type: 'Raveling',
      description: 'Deterioration of surface asphalt',
      accuracy: 5
    }, {
      type: 'Rutting',
      description: 'A depression or groove worn into a road',
      accuracy: 45
    }]
  }, {
    id: 9129393,
    score: 8,
    date: new Date(),
    image: '../../assets/joint-reflection.jpg',
    details: [{
      type: 'Alligator Cracking',
      description: 'Cracks forming a pattern that looks like scales',
      accuracy: 20
    }, {
      type: 'Longitudinal Cracking',
      description: 'Cracking that runs parallel along the road',
      accuracy: 50
    }, {
      type: 'Slippage Cracking',
      description: 'Crescent or half-moon shaped cracks',
      accuracy: 12
    }, {
      type: 'Transverse Cracking',
      description: 'Thermal cracks perpendicular to the pavement’s centerline',
      accuracy: 70
    }, {
      type: 'Asphalt Depression',
      description: 'Areas with slightly lower elevations than the surrounding pavement',
      accuracy: 13
    }, {
      type: 'Asphalt Upheaval',
      description: 'Localized upward movement in a pavement',
      accuracy: 11
    }, {
      type: 'Joint Reflection',
      description: 'Cracks in a flexible overlay over an existing crack or joint',
      accuracy: 93
    }, {
      type: 'Pot Hole',
      description: 'A depression or hollow in a road surface',
      accuracy: 2
    }, {
      type: 'Raveling',
      description: 'Deterioration of surface asphalt',
      accuracy: 7
    }, {
      type: 'Rutting',
      description: 'A depression or groove worn into a road',
      accuracy: 7
    }]
  }, {
    id: 9129394,
    score: 6,
    date: new Date(),
    image: '../../assets/longitudinal-crack.jpg',
    details: [{
      type: 'Alligator Cracking',
      description: 'Cracks forming a pattern that looks like scales',
      accuracy: 2
    }, {
      type: 'Longitudinal Cracking',
      description: 'Cracking that runs parallel along the road',
      accuracy: 95
    }, {
      type: 'Slippage Cracking',
      description: 'Crescent or half-moon shaped cracks',
      accuracy: 7
    }, {
      type: 'Transverse Cracking',
      description: 'Thermal cracks perpendicular to the pavement’s centerline',
      accuracy: 20
    }, {
      type: 'Asphalt Depression',
      description: 'Areas with slightly lower elevations than the surrounding pavement',
      accuracy: 15
    }, {
      type: 'Asphalt Upheaval',
      description: 'Localized upward movement in a pavement',
      accuracy: 14
    }, {
      type: 'Joint Reflection',
      description: 'Cracks in a flexible overlay over an existing crack or joint',
      accuracy: 13
    }, {
      type: 'Pot Hole',
      description: 'A depression or hollow in a road surface',
      accuracy: 5
    }, {
      type: 'Raveling',
      description: 'Deterioration of surface asphalt',
      accuracy: 7
    }, {
      type: 'Rutting',
      description: 'A depression or groove worn into a road',
      accuracy: 3
    }]
  }, {
    id: 9129395,
    score: 3,
    date: new Date(),
    image: '../../assets/pot-hole(1).jpg',
    details: [{
      type: 'Alligator Cracking',
      description: 'Cracks forming a pattern that looks like scales',
      accuracy: 60
    }, {
      type: 'Longitudinal Cracking',
      description: 'Cracking that runs parallel along the road',
      accuracy: 5
    }, {
      type: 'Slippage Cracking',
      description: 'Crescent or half-moon shaped cracks',
      accuracy: 7
    }, {
      type: 'Transverse Cracking',
      description: 'Thermal cracks perpendicular to the pavement’s centerline',
      accuracy: 4
    }, {
      type: 'Asphalt Depression',
      description: 'Areas with slightly lower elevations than the surrounding pavement',
      accuracy: 72
    }, {
      type: 'Asphalt Upheaval',
      description: 'Localized upward movement in a pavement',
      accuracy: 7
    }, {
      type: 'Joint Reflection',
      description: 'Cracks in a flexible overlay over an existing crack or joint',
      accuracy: 5
    }, {
      type: 'Pot Hole',
      description: 'A depression or hollow in a road surface',
      accuracy: 98
    }, {
      type: 'Raveling',
      description: 'Deterioration of surface asphalt',
      accuracy: 63
    }, {
      type: 'Rutting',
      description: 'A depression or groove worn into a road',
      accuracy: 81
    }]
  }, {
    id: 9129396,
    score: 7,
    date: new Date(),
    image: '../../assets/raveling.jpg',
    details: [{
      type: 'Alligator Cracking',
      description: 'Cracks forming a pattern that looks like scales',
      accuracy: 67
    }, {
      type: 'Longitudinal Cracking',
      description: 'Cracking that runs parallel along the road',
      accuracy: 23
    }, {
      type: 'Slippage Cracking',
      description: 'Crescent or half-moon shaped cracks',
      accuracy: 55
    }, {
      type: 'Transverse Cracking',
      description: 'Thermal cracks perpendicular to the pavement’s centerline',
      accuracy: 13
    }, {
      type: 'Asphalt Depression',
      description: 'Areas with slightly lower elevations than the surrounding pavement',
      accuracy: 34
    }, {
      type: 'Asphalt Upheaval',
      description: 'Localized upward movement in a pavement',
      accuracy: 11
    }, {
      type: 'Joint Reflection',
      description: 'Cracks in a flexible overlay over an existing crack or joint',
      accuracy: 8
    }, {
      type: 'Pot Hole',
      description: 'A depression or hollow in a road surface',
      accuracy: 40
    }, {
      type: 'Raveling',
      description: 'Deterioration of surface asphalt',
      accuracy: 91
    }, {
      type: 'Rutting',
      description: 'A depression or groove worn into a road',
      accuracy: 19
    }]
  }, {
    id: 9129397,
    score: 3,
    date: new Date(),
    image: '../../assets/rutting.jpg',
    details: [{
      type: 'Alligator Cracking',
      description: 'Cracks forming a pattern that looks like scales',
      accuracy: 3
    }, {
      type: 'Longitudinal Cracking',
      description: 'Cracking that runs parallel along the road',
      accuracy: 25
    }, {
      type: 'Slippage Cracking',
      description: 'Crescent or half-moon shaped cracks',
      accuracy: 45
    }, {
      type: 'Transverse Cracking',
      description: 'Thermal cracks perpendicular to the pavement’s centerline',
      accuracy: 13
    }, {
      type: 'Asphalt Depression',
      description: 'Areas with slightly lower elevations than the surrounding pavement',
      accuracy: 75
    }, {
      type: 'Asphalt Upheaval',
      description: 'Localized upward movement in a pavement',
      accuracy: 16
    }, {
      type: 'Joint Reflection',
      description: 'Cracks in a flexible overlay over an existing crack or joint',
      accuracy: 7
    }, {
      type: 'Pot Hole',
      description: 'A depression or hollow in a road surface',
      accuracy: 58
    }, {
      type: 'Raveling',
      description: 'Deterioration of surface asphalt',
      accuracy: 14
    }, {
      type: 'Rutting',
      description: 'A depression or groove worn into a road',
      accuracy: 95
    }]
  }, {
    id: 9129398,
    score: 6,
    date: new Date(),
    image: '../../assets/slippage_cracking.jpg',
    details: [{
      type: 'Alligator Cracking',
      description: 'Cracks forming a pattern that looks like scales',
      accuracy: 80
    }, {
      type: 'Longitudinal Cracking',
      description: 'Cracking that runs parallel along the road',
      accuracy: 15
    }, {
      type: 'Slippage Cracking',
      description: 'Crescent or half-moon shaped cracks',
      accuracy: 94
    }, {
      type: 'Transverse Cracking',
      description: 'Thermal cracks perpendicular to the pavement’s centerline',
      accuracy: 13
    }, {
      type: 'Asphalt Depression',
      description: 'Areas with slightly lower elevations than the surrounding pavement',
      accuracy: 66
    }, {
      type: 'Asphalt Upheaval',
      description: 'Localized upward movement in a pavement',
      accuracy: 13
    }, {
      type: 'Joint Reflection',
      description: 'Cracks in a flexible overlay over an existing crack or joint',
      accuracy: 5
    }, {
      type: 'Pot Hole',
      description: 'A depression or hollow in a road surface',
      accuracy: 53
    }, {
      type: 'Raveling',
      description: 'Deterioration of surface asphalt',
      accuracy: 60
    }, {
      type: 'Rutting',
      description: 'A depression or groove worn into a road',
      accuracy: 49
    }]
  }, {
    id: 9129399,
    score: 9,
    date: new Date(),
    image: '../../assets/transverse-crack.jpg',
    details: [{
      type: 'Alligator Cracking',
      description: 'Cracks forming a pattern that looks like scales',
      accuracy: 4
    }, {
      type: 'Longitudinal Cracking',
      description: 'Cracking that runs parallel along the road',
      accuracy: 20
    }, {
      type: 'Slippage Cracking',
      description: 'Crescent or half-moon shaped cracks',
      accuracy: 5
    }, {
      type: 'Transverse Cracking',
      description: 'Thermal cracks perpendicular to the pavement’s centerline',
      accuracy: 98
    }, {
      type: 'Asphalt Depression',
      description: 'Areas with slightly lower elevations than the surrounding pavement',
      accuracy: 3
    }, {
      type: 'Asphalt Upheaval',
      description: 'Localized upward movement in a pavement',
      accuracy: 30
    }, {
      type: 'Joint Reflection',
      description: 'Cracks in a flexible overlay over an existing crack or joint',
      accuracy: 83
    }, {
      type: 'Pot Hole',
      description: 'A depression or hollow in a road surface',
      accuracy: 3
    }, {
      type: 'Raveling',
      description: 'Deterioration of surface asphalt',
      accuracy: 7
    }, {
      type: 'Rutting',
      description: 'A depression or groove worn into a road',
      accuracy: 4
    }]
  }];

  selectedIndex = 0;

  constructor() { }

  ngOnInit() {
    this.selectedSegment = this.segments[0];
  }

  selectSegment(direction: number): void {
    if ((this.selectedIndex + direction) >= 0 && (this.selectedIndex + direction) < this.segments.length) {
      this.selectedIndex += direction;
      this.selectedSegment = this.segments[this.selectedIndex];
    }
  }
}
